const express = require('express');
const { randomBytes } = require('crypto');
const bodyParser = require('body-parser');

const cors = require('cors');
const axios = require('axios');
const app = express();
app.use(bodyParser.json());
app.use(cors());

const commentsByPostsId = {};

app.get('/posts/:id/comments', (req, res) => {
    res.json(commentsByPostsId[req.params.id] || []);
});

app.post('/posts/:id/comments', async (req, res) => {
    const commentId = randomBytes(4).toString('hex');
    const {content} = req.body;
    const comments = commentsByPostsId[req.params.id] || [];
    comments.push({id: commentId, content, status: 'pending'});
    commentsByPostsId[req.params.id] = comments;
    await axios.post('http://event-bus-srv:4005/events',{
        type: 'CommentCreated',
        data: {
            postId: req.params.id,
            id: commentId,
            content,
            status: 'pending'
        }
    })
    res.status(201).json(comments);
});

app.post('/events', async (req, res) => {
    const {type, data} = req.body;
    if(type === 'CommentModerated'){
        const {postId, id, status, content} = data;
        const comments = commentsByPostsId[postId];
        const comment = comments.find(c => c.id === id);
        comment.status = status;
        await axios.post('http://event-bus-srv:4005/events', {
            type: 'CommentUpdated',
            data: {
                postId,
                id,
                status,
                content
            }
        })
    }
    res.send('ok');
});

app.listen(4001, () => {
    console.log('Listening on port 4001');
});