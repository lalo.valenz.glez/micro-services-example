const express = require('express');
const { randomBytes } = require('crypto');
const bodyParser = require('body-parser');
const cors = require('cors');
const axios = require('axios');
const app = express();
app.use(bodyParser.json());
app.use(cors());

const posts = {};

app.post('/posts/create', async (req, res) => {
    const id = randomBytes(4).toString('hex');
    const {title} = req.body;
    posts[id] = {
        id,title
    }
    await axios.post('http://event-bus-srv:4005/events', {
        type: 'PostCreated',
        data: {id, title}
    })
    res.status(201).json(posts[id]);
});

app.post('/events', (req, res) => {
    const {type, data} = req.body;
    console.log('received event');
    console.log(type, data);
    res.send('ok');
});

app.listen(4000, () => {
    console.log('v55');
    console.log('Listening on port 4000');
});